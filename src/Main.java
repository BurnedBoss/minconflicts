import java.util.Random;
import java.util.Scanner;

public class Main {
    private int size;
    private int[] rowsIndexes;
    private int[] conflictsForQueen;

    private void init() {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        size = scanner.nextInt();
        rowsIndexes = new int[size];
        conflictsForQueen = new int[size];
        for (int i = 0; i < size; i++) {
            rowsIndexes[i] = random.nextInt(size);
        }

        for(int i = 0; i < size; i++) {
            for (int j = i + 1; j < size; j++) {
                if (queensInConflicts(rowsIndexes[i],i ,rowsIndexes[j],j)) {
                    conflictsForQueen[i]++;
                    conflictsForQueen[j]++;
                }
            }
        }
    }

    private void step() {
        int max = 0;
        int maxCol = 0;
        for(int i = 0; i < size; i++) {
            if(conflictsForQueen[i] > max) {
                max = conflictsForQueen[i];
                maxCol = i;
            }
        }
        if ( max == 0) {
            //printBoard();
        } else {
            int minConflicts = size;
            int minRow = 0;
            for (int r = 0; r < size; r++) {
                int value = conflictsForPosition(r, maxCol);
                if (minConflicts > value) {
                    minConflicts = value;
                    minRow = r;
                }
                else if (minConflicts == value && r > conflictsForQueen[r]) {
                    minRow = r;
                }

            }

            for (int i = 0; i < size; i++) {
                if (i != maxCol && queensInConflicts(rowsIndexes[i], i, rowsIndexes[maxCol], maxCol) &&
                        !queensInConflicts(rowsIndexes[i], i, minRow, maxCol)) {
                    conflictsForQueen[i]--;
                }
            }

            rowsIndexes[maxCol] = minRow;
            conflictsForQueen[maxCol] = minConflicts;
        }
    }
    private boolean haveZeroConflicts() {
        for (int i = 0; i < size; i++) {
            if (conflictsForQueen[i] != 0) {
                return false;
            }
        }
        return true;
    }
    private int conflictsForPosition(int row, int col) {
        int count = 0;
        for (int i = 0; i < size; i++) {
            if (i != col && queensInConflicts(row, col, rowsIndexes[i], i)) {
                count++;
            }
        }
        return count;
    }

    private boolean queensInConflicts(int row1, int col1, int row2, int col2) {
        return row1 == row2 || col1 == col2 || Math.abs(row1 - row2) == Math.abs(col1 - col2);
    }

    private void printBoard() {
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                if (i == rowsIndexes[j]) {
                    System.out.print("*");
                } else {
                    System.out.print("_");
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Main m = new Main();
        m.init();
        /* while(!m.haveZeroConflicts()) {
            m.step();
        }*/

        for (int i = 0; i < 100; i++ ) {
            m.step();
        }
        m.printBoard();
        System.out.println();
        for(int i = 0; i < m.size; i++) {
            System.out.println(m.conflictsForQueen[i]);
        }
    }
}